import React, {Component} from 'react';
import {
  DeviceEventEmitter,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import Beacons from 'react-native-beacons-manager';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';


Beacons.detectIBeacons();
Beacons.setForegroundScanPeriod(10000);
Beacons.setBackgroundScanPeriod(10000);
Beacons.setBackgroundBetweenScanPeriod(10000);
let isStartBeaconListener = false;
const UUID = '54455354-4A55-4A55-0000-000000000000';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uuid: UUID,
      identifier: 'TPC BLUE',
      majors:[
        903,
        3,
        1,
        2,
        2,
        1009
      ]
    };
  }

  beaconsDidEnterEvent = null;
  beaconsDidLeaveEvent = null;

  async componentDidMount() {
    if(!isStartBeaconListener) {
      await this.state.majors.map(async major => {
        await Beacons.startBeaconServices({identifier: this.state.identifier, uuid: this.state.uuid, major: major});
      })

      this.beaconsDidEnterEvent = DeviceEventEmitter.addListener('regionDidEnter', async ({identifier, uuid, minor, major}) => {
            console.log('regionDidEnter: ', {identifier, uuid, minor, major});
          },
      );

      this.beaconsDidLeaveEvent = DeviceEventEmitter.addListener('regionDidExit', async ({identifier, uuid, minor, major}) => {
        console.log('regionDidExit: ', {identifier, uuid, minor, major});
      })
      isStartBeaconListener = true;
    }
  }

  render() {
    return (
        <>
          <StatusBar barStyle="dark-content"/>
          <SafeAreaView>
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}>
              <Header/>
              {global.HermesInternal == null ? null : (
                  <View style={styles.engine}>
                    <Text style={styles.footer}>Engine: Hermes</Text>
                  </View>
              )}
              <View style={styles.body}>
                <View style={styles.sectionContainer}>
                  <Text style={styles.sectionTitle}>Step One</Text>
                  <Text style={styles.sectionDescription}>
                    Edit <Text style={styles.highlight}>App.js</Text> to change this
                    screen and then come back to see your edits.
                  </Text>
                </View>
                <View style={styles.sectionContainer}>
                  <Text style={styles.sectionTitle}>See Your Changes</Text>
                  <Text style={styles.sectionDescription}>
                    <ReloadInstructions/>
                  </Text>
                </View>
                <View style={styles.sectionContainer}>
                  <Text style={styles.sectionTitle}>Debug</Text>
                  <Text style={styles.sectionDescription}>
                    <DebugInstructions/>
                  </Text>
                </View>
                <View style={styles.sectionContainer}>
                  <Text style={styles.sectionTitle}>Learn More</Text>
                  <Text style={styles.sectionDescription}>
                    Read the docs to discover what to do next:
                  </Text>
                </View>
                <LearnMoreLinks/>
              </View>
            </ScrollView>
          </SafeAreaView>
        </>
    );
  }
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
